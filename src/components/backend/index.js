const browser = require('webextension-polyfill');

function matchRuleShort(str, rule) {
  return new RegExp('^' + rule.split('*').join('.*') + '$').test(str);
}

let allowedUrls = [
  '*://avito.ru/*',
  '*://*.avito.ru/*',
  '*://avito.st/*',
  '*://*.avito.st/*',

  '*://whatleaks.com/*',
  '*://*.whatleaks.com/*',

  '*://browserleaks.com/*',
  '*://*.browserleaks.com/*',

  '*://whoer.net/*',
  '*://*.whoer.net/*',

  '*://2ip.ru/*',
  '*://*.2ip.ru/*',

  '*://f.vision/*',
  '*://*.f.vision/*',

  '*://mail.ru/*',
  '*://*.mail.ru/*',

  '*://yandex.ru/*',
  '*://*.yandex.ru/*',

  '*://rambler.ru/*',
  '*://*.rambler.ru/*',

  '*://browserspy.dk/*',
  '*://*.browserspy.dk/*',

  '*://youla.ru/*',
  '*://*.youla.ru/*',

  '*://youla.io/*',
  '*://*.youla.io/*',
  '*://vboro.de/*',
  '*://*.vboro.de/*',

  '*://olx.ua/*',
  '*://*.olx.ua/*',
  '*://olx.kz/*',
  '*://*.olx.kz/*',
  '*://olx.uz/*',
  '*://*.olx.uz/*',
  '*://hcaptcha.com/*',
  '*://*.hcaptcha.com/*'
];

function getProxyHost(host) {
  let _host = host;

  if (host.indexOf('clearproxy.ru') > -1) {
    let time = +new Date();
    time = time / 1000;
    time = Math.floor(time / 300);

    _host = time + '.clearproxy.ru';
  }

  console.log('proxy host', _host);
  return _host;
}

function getCurrentProxy() {
  return new Promise(resolve => {
    browser.storage.local.get(['profile']).then((data = {}) => {
      let profile = data.profile ? data.profile.data : null;

      if (profile && profile.proxy_host) {
        resolve({
          proxy_host: getProxyHost(profile.proxy_host),
          proxy_port: parseInt('' + profile.proxy_port),
          username: profile.proxy_user,
          password: profile.proxy_pass
        });
      } else {
        resolve({});
      }
    });
  });
}

const pendingRequests = [];

function requestCompleted(requestDetails) {
  const index = pendingRequests.indexOf(requestDetails.requestId);
  if (index > -1) {
    pendingRequests.splice(index, 1);
  }
}

function provideCredentials(requestDetails) {
  console.log('provideCredentials start');
  return new Promise(resolve => {
    if (pendingRequests.indexOf(requestDetails.requestId) !== -1) {
      console.log('provideCredentials cancel');

      resolve({ cancel: true });
    } else {
      pendingRequests.push(requestDetails.requestId);
      return getCurrentProxy().then(proxy => {
        if (
          parseInt(requestDetails.challenger.port) ===
            parseInt(proxy.proxy_port) &&
          proxy.username
        ) {
          console.log(
            'provideCredentials resolve ' +
              proxy.username +
              ' ' +
              proxy.password
          );

          resolve({
            authCredentials: {
              username: proxy.username,
              password: proxy.password
            }
          });
        } else {
          console.log('provideCredentials resolve nothing');

          resolve();
        }
      });
    }
  });
}

let proxyOptions = {};
let handleProxyRequest = requestInfo => {
  console.log(`Proxying: ${requestInfo.url} ${proxyOptions.proxy_host}`);
  return {
    type: 'http',
    host: getProxyHost(proxyOptions.proxy_host),
    port: parseInt('' + proxyOptions.proxy_port)
  };
};

function proxyInit() {
  console.log('proxyInit');
  browser.storage.local.get(['profile']).then((data = {}) => {
    proxyOptions = data.profile ? data.profile.data : null;
    console.log('proxyInit profile', proxyOptions);

    if (proxyOptions && proxyOptions.proxy_host) {
      if (browser.proxy.onRequest && browser.proxy.onRequest.addListener) {
        console.log('[firefox] proxyInit host', proxyOptions.proxy_host);

        browser.proxy.onRequest.addListener(handleProxyRequest, {
          urls: allowedUrls
        });
      } else {
        console.log('[chrome] proxyInit host', proxyOptions.proxy_host);
        let e = {
          mode: 'pac_script',
          pacScript: {
            data:
              'function FindProxyForURL(url, host) {\n' +
              '  if (shExpMatch(host, "(money.yandex.ru)"))\n' +
              "    return 'DIRECT';\n" +
              '  if (shExpMatch(host, "(127.0.0.1|*.browserspy.dk|browserspy.dk|*.google.com|google.com|*.google.ru|google.ru|*.rambler.ru|rambler.ru|*.yandex.ru|yandex.ru|*.mail.ru|mail.ru|*.f.vision|f.vision|*.2ip.ru|2ip.ru|*.whoer.net|whoer.net|*.browserleaks.com|browserleaks.com|*.whatleaks.com|whatleaks.com|*.avito.ru|avito.ru|*.avito.st|avito.st|youla.ru|*.youla.ru|youla.io|*.youla.io|vboro.de|*.vboro.de|*.olx.ua|olx.ua|*.olx.kz|olx.kz|*.olx.uz|olx.uz|hcaptcha.com|*.hcaptcha.com)"))\n' +
              "    return 'PROXY " +
              getProxyHost(proxyOptions.proxy_host) +
              ':' +
              parseInt('' + proxyOptions.proxy_port) +
              "';\n" +
              "  return 'DIRECT';\n" +
              '}'
          }
        };

        browser.proxy.settings.set({ value: e, scope: 'regular' });
      }
    } else {
      if (browser.proxy.onRequest && browser.proxy.onRequest.removeListener) {
        console.log('[firefox] proxyInit clear proxy settings');

        browser.proxy.onRequest.removeListener(handleProxyRequest);
      } else {
        console.log('[chrome] proxyInit clear proxy settings');

        browser.proxy.settings.clear({});
      }
    }
  });
}

if (!(browser.proxy.onRequest && browser.proxy.onRequest.removeListener)) {
  console.log('[chrome] proxy auth init');
  browser.webRequest.onAuthRequired.addListener(
    (requestDetails, chromeCallback) =>
      provideCredentials(requestDetails).then(result => chromeCallback(result)),
    { urls: allowedUrls },
    ['asyncBlocking']
  );
} else {
  console.log('[firefox] proxy auth init');
  browser.webRequest.onAuthRequired.addListener(
    provideCredentials,
    { urls: allowedUrls },
    ['blocking']
  );
}

proxyInit();

browser.webRequest.onCompleted.addListener(requestCompleted, {
  urls: allowedUrls
});

browser.webRequest.onErrorOccurred.addListener(requestCompleted, {
  urls: allowedUrls
});

let ua = '';

setInterval(getUa, 500);
function getUa() {
  browser.storage.local.get(['useragent']).then((data = {}) => {
    ua = data.useragent || '';
  });
}

const requestInterceptor = file => {
  let types = {
    requestHeaders: file.requestHeaders
  };
  if (
    file &&
    file.url &&
    file.requestHeaders &&
    file.requestHeaders.length > 0
  ) {
    for (const r of file.requestHeaders) {
      if ('User-Agent' === r.name) {
        ua = ua.replace(/en_US/gi, 'ru_RU').replace(/en-US/gi, 'ru-RU');
        r.value = ua;
      }
      if ('Accept-Language' === r.name) {
        // console.log('r.name', r.name);
        r.value = 'ru-RU,ru';
      }
    }
  }
  return types;
};
browser.webRequest.onBeforeSendHeaders.addListener(
  requestInterceptor,
  {
    urls: allowedUrls
  },
  ['requestHeaders', 'blocking']
);

const randomCouchString = () => {
  let text = '';
  const raw_composed_type = 'abcdefghijklmnopqrstuvwxyz';
  let s = 0;
  for (; s < 5; s++) {
    text =
      text +
      raw_composed_type.charAt(
        Math.floor(Math.random() * raw_composed_type.length)
      );
  }
  return text;
};
browser.storage.local.set({
  docId: randomCouchString()
});

browser.privacy.network.webRTCIPHandlingPolicy.set({
  value: 'disable_non_proxied_udp'
});

browser.runtime.onMessage.addListener(function(request) {
  let isEnabled, lat, lng, accuracy;

  switch (request.type) {
    case 'setproxy':
      proxyInit();
      break;
    case 'fakegeo':
      // eslint-disable-next-line no-case-declarations
      lat = localStorage['lat'];
      lng = localStorage['lng'];
      accuracy = localStorage['accuracy'];
      isEnabled = !!lat && !!lng && localStorage['isEnabled'] === 'true';
      if (!isEnabled) {
        return Promise.resolve({});
      }

      return Promise.resolve({
        isEnabled: isEnabled,
        lat: parseFloat(lat),
        lng: parseFloat(lng),
        accuracy: parseFloat(accuracy)
      });
    case 'activeChanged':
      isEnabled = localStorage['isEnabled'] === 'true';
      browser.windows.getAll({ populate: true }).then(windows => {
        for (let i = 0; i < windows.length; ++i) {
          let window = windows[i];
          for (let j = 0; j < window.tabs.length; ++j) {
            let tab = window.tabs[j];
            browser.tabs.sendMessage(
              tab.id,
              {
                type: 'updateFakeGeo',
                isEnabled: isEnabled,
                lat: localStorage['lat'],
                lng: localStorage['lng'],
                accuracy: localStorage['accuracy']
              },
              null
            );
          }
        }
      });
      break;
  }
});

let df = new Date().getTimezoneOffset();

let onCommitted = ({ url, tabId, frameId }) => {
  if (url && url.startsWith('http')) {
    let match = false;
    for (let i in allowedUrls) {
      let result = matchRuleShort(url, allowedUrls[i]);
      if (result) {
        match = true;
        break;
      }
    }

    if (match) {
      let location = localStorage.getItem('location');
      const standard = localStorage.getItem('standard');
      const daylight = localStorage.getItem('daylight');

      let offset = localStorage.getItem('offset') || 0;
      let msg = localStorage.getItem('isDST') === 'false' ? standard : daylight;

      browser.tabs.executeScript(tabId, {
        runAt: 'document_start',
        frameId,
        matchAboutBlank: true,
        code: `document.documentElement.appendChild(Object.assign(document.createElement('script'), {
        textContent: 'Date.prefs = ["${location}", ${-1 *
          offset}, ${df}, "${msg}"];'
      })).remove();`
      });
    }
  }
};

let update = () => {
  browser.webNavigation.onCommitted.addListener(onCommitted);
};

browser.storage.onChanged.addListener(() => {
  update();
});

update();

var script = '';

function defineGetter(object, name, returnValue) {
  script += `Object.defineProperty(${object}, '${name}',  { configurable: true, enumerable: true, get: function() { return ${returnValue}; } });`;
}

// function updateCanvas(rand) {
//   if (window.location.hostname.indexOf('youla') > -1) return;
//   if (window.location.href.indexOf('settings') > -1) return;
//
//   console.log('updateCanvas', rand);
//   script += `HTMLCanvasElement.prototype.toDataURL = function() {console.log('toDataURL'); return '${rand}';};`;
//   script += `HTMLCanvasElement.prototype.toBlob = function() {console.log('toBlob');return '${rand}';};`;
//   script += `HTMLCanvasElement.mozGetAsFile = function() {console.log('mozGetAsFile');return '${rand}';};`;
//   script += `window.HTMLCanvasElement.prototype.toDataURL = function() {console.log('window toDataURL');return '${rand}';};`;
//   script += `window.HTMLCanvasElement.prototype.toBlob = function() {console.log('window toBlob');return '${rand}';};`;
//   script += `window.HTMLCanvasElement.mozGetAsFile = function() {console.log('window mozGetAsFile');return '${rand}';};`;
//
//   // script += "Object.defineProperty(HTMLCanvasElement.prototype, 'toDataURL', { set: function() {}, get: function() {} });";
//
//   // script += "Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', { set: function() {} });";
// }

// function updateStyleDeclaration() {
//   script +=
//     "Object.defineProperty(CSSStyleDeclaration.prototype, 'fontFamily', { set: function() {}, get: function() {} });";
//   script +=
//     "Object.defineProperty(CSSStyleDeclaration.prototype, 'font-Family', { set: function() {}, get: function() {} });";
// }

// function updateTimezone() {
//   script += 'Date.prototype.getTimezoneOffset = function() {return 0;};';
// }
function noMediaDevices() {
  script +=
    'if(navigator.mediaDevices) { navigator.mediaDevices.getUserMedia = function() {return 0;}; } ';
  script +=
    'if(navigator.mediaDevices) { navigator.mediaDevices.enumerateDevices = function() {return new Promise(function(r,re){re();});}; }';
}

function updateDocument(url) {
  script += `window.document.__defineGetter__('referrer', function() {return '${url}';});\n`;
}

function updateHistory() {
  script +=
    "History.prototype.__defineGetter__('length', function() {return 0;});\n";
}

function resetScript() {
  script = '';
}

function updateNavigator(userAgent) {
  defineGetter('window.navigator', 'appName', "'Netscape'");
  defineGetter('window.navigator', 'userAgent', "'" + userAgent + "'");

  defineGetter('window.navigator', 'language', "'ru-RU'");
  defineGetter('window.navigator', 'languages', "['ru-RU','ru']");
  defineGetter('window.navigator', 'platform', "'Win32'");
  defineGetter('window.navigator', 'plugins', '[]');
  defineGetter('window.navigator', 'product', "'Gecko'");
  defineGetter('window.navigator', 'productSub', "'20030107'");
  defineGetter('window.navigator', 'vendor', "'Google Inc.'");
  defineGetter('window.navigator', 'buildID', "''");
  defineGetter('window.navigator', 'id', "''");
  defineGetter('window.navigator', 'doNotTrack', 'true');
  defineGetter('window.navigator', 'vendorSub', "''");
  defineGetter('window.navigator', 'vibrate', "''");
  defineGetter('window.navigator', 'webkitPointer', "''");
  defineGetter('window.navigator', 'registerContentHandler', "''");
  defineGetter('window.navigator', 'registerProtocolHandler', "''");
  defineGetter('window.navigator', 'requestWakeLock', "''");
}
function updateNavigatorPlug() {
  defineGetter('window.navigator', 'plugins', '[]');
  defineGetter('window.navigator', 'webkitGetGamepads', "''");
  defineGetter('window.navigator', 'webkitGetUserMedia', "''");
  defineGetter('window.navigator', 'mediaDevices', '[]');
}
function updateNavigatorCpu() {
  defineGetter('window.navigator', 'hardwareConcurrency', '2');
  defineGetter('window.navigator', 'deviceMemory', '4');
}
function updateNavigatorNetwork() {
  defineGetter('window.navigator', 'appName', "'Netscape'");
  script += 'navigator.connection.rtt = function() {return 0;};';
}
function updateNavigatorAudio() {
  // defineGetter('window', 'OfflineAudioContext',"''");
  // defineGetter('window', 'webkitAudioContext', "''");
  // defineGetter('window', 'AudioContext', "''");
}

function createScript(userAgent, url) {
  resetScript();

  updateDocument(url);
  updateHistory();
  updateNavigator(userAgent);
  // updateCanvas(rand);
  // updateStyleDeclaration();
  // updateTimezone();
  noMediaDevices();
  return script;
}
function createPlugins(userAgent) {
  resetScript();
  updateNavigatorPlug(userAgent);
  return script;
}
function createCpu(userAgent) {
  resetScript();
  updateNavigatorCpu(userAgent);
  return script;
}
function createNetwork(userAgent) {
  resetScript();
  updateNavigatorNetwork(userAgent);
  return script;
}

function createAudio(userAgent) {
  resetScript();
  updateNavigatorAudio(userAgent);
  return script;
}

function updateContent(doc, userAgent, rand) {
  const documentElement = doc.head || doc.documentElement;
  const scriptElement = doc.createElement('script');

  scriptElement.textContent = createScript(userAgent, doc.URL, rand);
  documentElement.insertBefore(scriptElement, documentElement.firstChild);
  scriptElement.parentNode.removeChild(scriptElement);
}
function updatePlugins(doc, userAgent) {
  const documentElement = doc.head || doc.documentElement;
  const scriptElement = doc.createElement('script');

  scriptElement.textContent = createPlugins(userAgent, doc.URL);
  documentElement.insertBefore(scriptElement, documentElement.firstChild);
  scriptElement.parentNode.removeChild(scriptElement);
}
function updateCpu(doc, userAgent) {
  const documentElement = doc.head || doc.documentElement;
  const scriptElement = doc.createElement('script');

  scriptElement.textContent = createCpu(userAgent, doc.URL);
  documentElement.insertBefore(scriptElement, documentElement.firstChild);
  scriptElement.parentNode.removeChild(scriptElement);
}
function updateNetwork(doc, userAgent) {
  const documentElement = doc.head || doc.documentElement;
  const scriptElement = doc.createElement('script');

  scriptElement.textContent = createNetwork(userAgent, doc.URL);
  documentElement.insertBefore(scriptElement, documentElement.firstChild);
  scriptElement.parentNode.removeChild(scriptElement);
}

function updateAudio(doc, userAgent) {
  const documentElement = doc.head || doc.documentElement;
  const scriptElement = doc.createElement('script');
  try {
    scriptElement.textContent = createAudio(userAgent, doc.URL);
    documentElement.insertBefore(scriptElement, documentElement.firstChild);
    scriptElement.parentNode.removeChild(scriptElement);
  } catch (e) {
    /* Content Settings */
  }
}

//
// if (typeof module !== 'undefined') {
//   module.exports = updateContent;
// }

module.exports = {
  updateContent,
  updatePlugins,
  updateCpu,
  updateNetwork,
  updateAudio
};

import browser from 'webextension-polyfill';
const seedrandom = require('seedrandom');
import {
  updateContent,
  updatePlugins,
  updateCpu,
  updateNetwork,
  updateAudio
} from './define-getters';

import fakeGeoInit from './fake-geo.js';
import timezoneInit from './timezone.js';
// import webglInit from './override-webgl.js';
import canvasInject from './canvas.js';
import fontInject from './font.js';
import webglInject from './webgl.js';
import audioInject from './audio.js';
import webrtcInject from './webrtc.js';
import rectsInject from './client-rects.js';

console.log('[index.js]');

browser.storage.local.get(['profile', 'useragent']).then((data = {}) => {
  let profile = data.profile.data || { cid: Math.random() };
  let useragent = data.useragent || window.navigator.userAgent;
  const myrng = new seedrandom(profile.cid);
  console.log('seed random', myrng(), profile);

  canvasInject(myrng());
  rectsInject(myrng());
  fontInject(myrng());
  webglInject(myrng());
  audioInject(myrng());
  webrtcInject();

  fakeGeoInit();
  timezoneInit();
  // webglInit(myrng());

  updateContent(document, useragent, myrng());
  updatePlugins(document);
  updateCpu(document);
  updateNetwork(document);
  updateAudio(document);
});

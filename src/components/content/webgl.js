var inject = function(seed) {
  var config = {
    random: {
      value: function() {
        return seed;
      },
      item: function(e) {
        var rand = e.length * config.random.value();
        return e[Math.floor(rand)];
      },
      array: function(e) {
        var rand = config.random.item(e);
        return new Int32Array([rand, rand]);
      },
      items: function(e, n) {
        var length = e.length;
        var result = new Array(n);
        var taken = new Array(length);
        if (n > length) n = length;
        //
        while (n--) {
          var i = Math.floor(config.random.value() * length);
          result[n] = e[i in taken ? taken[i] : i];
          taken[i] = --length in taken ? taken[length] : length;
        }
        //
        return result;
      }
    },
    spoof: {
      webgl: {
        buffer: function(target) {
          const bufferData = target.prototype.bufferData;
          Object.defineProperty(target.prototype, 'bufferData', {
            value: function() {
              var index = Math.floor(config.random.value() * 10);
              var noise = 0.1 * config.random.value() * arguments[1][index];
              arguments[1][index] = arguments[1][index] + noise;
              window.top.postMessage('webgl-fingerprint-defender-alert', '*');
              //
              return bufferData.apply(this, arguments);
            }
          });
        },
        parameter: function(target) {
          // const getParameter = target.prototype.getParameter;
          Object.defineProperty(target.prototype, 'getParameter', {
            value: function() {
              var float32array = new Float32Array([1, 8192]);
              window.top.postMessage('webgl-fingerprint-defender-alert', '*');
              //
              if (arguments[0] === 3415) return 0;
              else if (arguments[0] === 3414) return 24;
              else if (arguments[0] === 35661)
                return config.random.items([128, 192, 256]);
              else if (arguments[0] === 3386)
                return config.random.array([8192, 16384, 32768]);
              else if (arguments[0] === 36349 || arguments[0] === 36347)
                return config.random.item([4096, 8192]);
              else if (arguments[0] === 34047 || arguments[0] === 34921)
                return config.random.items([2, 4, 8, 16]);
              else if (
                arguments[0] === 7937 ||
                arguments[0] === 33901 ||
                arguments[0] === 33902
              )
                return float32array;
              else if (
                arguments[0] === 34930 ||
                arguments[0] === 36348 ||
                arguments[0] === 35660
              )
                return config.random.item([16, 32, 64]);
              else if (
                arguments[0] === 34076 ||
                arguments[0] === 34024 ||
                arguments[0] === 3379
              )
                return config.random.item([16384, 32768]);
              else if (
                arguments[0] === 3413 ||
                arguments[0] === 3412 ||
                arguments[0] === 3411 ||
                arguments[0] === 3410 ||
                arguments[0] === 34852
              ) {
                return config.random.item([2, 4, 8, 16]);
              } else if (arguments[0] === 37446) {
                return config.random.item([
                  'ANGLE (NVIDIA GeForce GTX 1050 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'PowerVR SGX 543',
                  'ANGLE (Intel(R) HD Graphics 4000 Direct3D11 vs_5_0 ps_5_0)',
                  'PowerVR SGX 554',
                  'Adreno (TM) 506',
                  'ANGLE (NVIDIA GeForce GTX 1060 6GB Direct3D11 vs_5_0 ps_5_0)',
                  'Mali-T830',
                  'Mali-G71',
                  'ANGLE (Intel(R) HD Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'Mali-G72',
                  'ANGLE (Intel(R) UHD Graphics 630 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics 630 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) UHD Graphics 620 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics 620 Direct3D11 vs_5_0 ps_5_0)',
                  'Intel(R) UHD Graphics 617',
                  'ANGLE (Intel(R) HD Graphics 4600 Direct3D11 vs_5_0 ps_5_0)',
                  'Intel(R) HD Graphics 6000',
                  'Apple A7 GPU',
                  'ANGLE (Intel(R) HD Graphics 520 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics Family Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 1070 Direct3D11 vs_5_0 ps_5_0)',
                  'Mali-T720',
                  'Adreno (TM) 512',
                  'Mali-G51',
                  'Adreno (TM) 505',
                  'Adreno (TM) 630',
                  'Mali-G76',
                  'ANGLE (NVIDIA GeForce GTX 1080 Direct3D11 vs_5_0 ps_5_0)',
                  'Apple A8X GPU',
                  'ANGLE (Intel(R) HD Graphics 5500 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 1060 3GB Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 970 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 1050 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 750 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics 530 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 760 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics Family Direct3D9Ex vs_3_0 ps_3_0)',
                  'Apple M1',
                  'ANGLE (Intel(R) HD Graphics 3000 Direct3D11 vs_4_1 ps_4_1)',
                  'ANGLE (Intel(R) HD Graphics 3000 Direct3D9Ex vs_3_0 ps_3_0)',
                  'Adreno (TM) 308',
                  'ANGLE (NVIDIA GeForce GTX 960 Direct3D11 vs_5_0 ps_5_0)',
                  'Adreno (TM) 640',
                  'ANGLE (Radeon RX 580 Series Direct3D11 vs_5_0 ps_5_0)',
                  'Mali-400 MP',
                  'Mali-T880',
                  'Adreno (TM) 540',
                  'Apple A9 GPU',
                  'ANGLE (AMD Radeon(TM) Vega 8 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'Adreno (TM) 509',
                  'Intel HD Graphics 4000 OpenGL Engine',
                  'Apple A10 GPU',
                  'ANGLE (Radeon RX 570 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 1080 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon R9 200 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 660 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 650 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics 4000 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce RTX 2060 Direct3D11 vs_5_0 ps_5_0)',
                  'Mali-T860',
                  'PowerVR Rogue GE8320',
                  'Adreno (TM) 610',
                  'ANGLE (AMD Radeon(TM) R5 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 1070 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 730 Direct3D11 vs_5_0 ps_5_0)',
                  'Intel(R) Iris(TM) Plus Graphics 640',
                  'Adreno (TM) 618',
                  'Intel Iris OpenGL Engine',
                  'Adreno (TM) 306',
                  'Adreno (TM) 530',
                  'Intel(R) UHD Graphics 630',
                  'Mali-G76 MC4',
                  'ANGLE (Mobile Intel(R) 4 Series Express Chipset Family Direct3D9Ex vs_3_0 ps_3_0)',
                  'Apple A8 GPU',
                  'ANGLE (AMD Radeon R7 200 Series Direct3D11 vs_5_0 ps_5_0)',
                  'Intel HD Graphics 5000 OpenGL Engine',
                  'ANGLE (Intel(R) HD Graphics Direct3D11 vs_4_1 ps_4_1)',
                  'ANGLE (NVIDIA GeForce GTX 950 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce RTX 2070 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon(TM) R4 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'Intel(R) Iris(TM) Graphics 6100',
                  'ANGLE (NVIDIA GeForce GTX 550 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 630 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 1030 Direct3D11 vs_5_0 ps_5_0)',
                  'Intel Iris Pro OpenGL Engine',
                  'Adreno (TM) 616',
                  'ANGLE (AMD Radeon HD 7700 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 1660 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 750 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce RTX 2070 SUPER Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 770 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 710 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce 210  Direct3D11 vs_4_1 ps_4_1)',
                  'ANGLE (AMD Radeon HD 7800 Series Direct3D11 vs_5_0 ps_5_0)',
                  'Mali-G72 MP3',
                  'ANGLE (NVIDIA GeForce GTS 450 Direct3D11 vs_5_0 ps_5_0)',
                  'Adreno (TM) 508',
                  'ANGLE (NVIDIA GeForce GTX 1060 Direct3D11 vs_5_0 ps_5_0)',
                  'Mali-T760',
                  'ANGLE (NVIDIA GeForce GTX 650 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'Adreno (TM) 510',
                  'Mali-450 MP',
                  'Intel(R) HD Graphics 530',
                  'Adreno (TM) 405',
                  'Apple A9X GPU',
                  'Intel(R) Iris(TM) Graphics 550',
                  'ANGLE (AMD Radeon(TM) R7 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'Apple A11 GPU',
                  'ANGLE (AMD Radeon HD 7700 Series Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GTX 1650 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (ATI Mobility Radeon HD 5650 Direct3D9Ex vs_3_0 ps_3_0)',
                  'Adreno (TM) 612',
                  'Adreno (TM) 330',
                  'ANGLE (Intel(R) HD Graphics 4400 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Radeon (TM) RX 480 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'PowerVR Rogue GE8100',
                  'ANGLE (NVIDIA GeForce RTX 2060 SUPER Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics 610 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) Graphics Media Accelerator 3600 Series Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GTX 550 Ti Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon(TM) Vega 3 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Radeon RX 560 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 640 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce 7025 / NVIDIA nForce 630a Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GTX 980 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) G41 Express Chipset (Microsoft Corporation - WDDM 1.1) Direct3D9Ex vs_3_0 ps_3_0)',
                  'Microsoft Basic Render Driver',
                  'ANGLE (NVIDIA GeForce GTX 580 Direct3D11 vs_5_0 ps_5_0)',
                  'Apple A10X GPU',
                  'PowerVR SGX Doma',
                  'ANGLE (AMD 760G Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GT 630 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (Intel(R) HD Graphics 4600 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon (TM) R9 380 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon R7 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce RTX 2080 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) G41 Express Chipset Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (Radeon RX 590 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) UHD Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'AMD Radeon Pro 560X OpenGL Engine',
                  'Intel(R) Iris(TM) Plus Graphics 655',
                  'ANGLE (AMD Radeon(TM) RX Vega 11 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 650 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce 9500 GT Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon HD 6800 Series Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (ATI Radeon HD 4300/4500 Series Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce RTX 2080 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Radeon (TM) RX 470 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'AMD Radeon Pro 555X OpenGL Engine',
                  'Intel(R) Iris(TM) Plus Graphics OpenGL Engine',
                  'ANGLE (AMD Radeon HD 6670 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 430 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon HD 5700 Series Direct3D11 vs_5_0 ps_5_0)',
                  'Intel(R) Iris(TM) Plus Graphics 645',
                  'ANGLE (NVIDIA GeForce GT 610 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) Q45/Q43 Express Chipset Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon HD 7640G Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GT 440 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon(TM) R2 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 560 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GTX 660 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (ATI Radeon HD 4600 Series Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce 9600 GT Direct3D11 vs_4_0 ps_4_0)',
                  'ANGLE (Intel(R) HD Graphics 510 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 610 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GT 620 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon RX 5700 XT Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon(TM) R3 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'Adreno (TM) 305',
                  'ANGLE (Intel(R) UHD Graphics 600 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon HD 7310 Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'Intel(R) Iris(TM) Plus Graphics 650',
                  'ANGLE (NVIDIA GeForce GT 240 Direct3D11 vs_4_1 ps_4_1)',
                  'ANGLE (AMD Radeon HD 6290 Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (ATI Radeon HD 5450 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GT 740 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (ATI Mobility Radeon HD 5470 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (Radeon(TM) RX 460 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 430 Direct3D9Ex vs_3_0 ps_3_0)',
                  'AMD Radeon R9 M370X OpenGL Engine',
                  'ANGLE (NVIDIA GeForce GTX 660 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) UHD Graphics 605 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 1660 SUPER Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 240 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce 310M Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GTX 460 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 980 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce RTX 2080 SUPER Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Mobility Radeon HD 5000 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTS 250 Direct3D11 vs_4_0 ps_4_0)',
                  'ANGLE (NVIDIA GeForce GTX 560 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce 9200 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (Microsoft Basic Render Driver Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GTX 670 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (Intel(R) HD Graphics Direct3D11 vs_4_0 ps_4_0)',
                  'ANGLE (NVIDIA GeForce GT 220 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon HD 6800 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon(TM) RX Vega 10 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon (TM) R7 360 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 440 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (ATI Radeon 3000 Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'AMD Radeon R9 M290X OpenGL Engine',
                  'ANGLE (NVIDIA GeForce 6150SE nForce 430 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon (TM) R9 200 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce GT 640 Direct3D9Ex vs_3_0 ps_3_0)',
                  'Intel(R) HD Graphics 630',
                  'ANGLE (AMD Radeon HD 7340 Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'Mali-G52 MC2',
                  'ANGLE (NVIDIA GeForce GTX 560 Ti Direct3D11 vs_5_0 ps_5_0)',
                  'NVIDIA GeForce GT 650M OpenGL Engine',
                  'ANGLE (AMD Radeon HD 5450 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon HD 6570 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon HD 6700 Series Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon R5 Graphics Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon HD 6450 Direct3D9Ex vs_3_0 ps_3_0)',
                  'AMD Radeon HD 6750M OpenGL Engine',
                  'ANGLE (AMD Radeon HD 6320 Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (Software Adapter Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (ATI Radeon HD 4200 Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (Intel(R) UHD Graphics 610 Direct3D11 vs_5_0 ps_5_0)',
                  'AMD Radeon Pro 580 OpenGL Engine',
                  'ANGLE (AMD Radeon HD 7800 Series Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (Mobile Intel(R) 965 Express Chipset Family Direct3D9Ex vs_3_0 ps_3_0)',
                  'NVIDIA GeForce GT 750M OpenGL Engine',
                  'ANGLE (NVIDIA GeForce 310M             Direct3D11 vs_4_1 ps_4_1)',
                  'Adreno (TM) 304',
                  'ANGLE (AMD Radeon HD 6310 Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon(TM) HD 6520G Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (AMD Radeon HD 6520G Direct3D11 vs_5_0 ps_5_0)',
                  'Apple A12 GPU',
                  'ANGLE (NVIDIA GeForce 9500 GT Direct3D11 vs_4_0 ps_4_0)',
                  'ANGLE (Intel(R) Graphics Media Accelerator HD Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (ATI Radeon HD 3200 Graphics Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GTX 745 Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (AMD Radeon HD 6500M/5600/5700 Series Direct3D9Ex vs_3_0 ps_3_0)',
                  'ANGLE (NVIDIA GeForce GTX 1650 SUPER Direct3D11 vs_5_0 ps_5_0)',
                  'ANGLE (NVIDIA GeForce 7025 / NVIDIA nForce 630a (Microsoft Corporation - WDDM) Direct3D9Ex vs_3_0 ps_3_0)'
                ]);
              } else if (arguments[0] === 37445) {
                return 'Google Inc.';
              } else {
                return config.random.item([
                  0,
                  2,
                  4,
                  8,
                  16,
                  32,
                  64,
                  128,
                  256,
                  512,
                  1024,
                  2048,
                  4096
                ]);
              }
              //
              // return getParameter.apply(this, arguments);
            }
          });
        }
      }
    }
  };
  //
  config.spoof.webgl.buffer(WebGLRenderingContext);
  config.spoof.webgl.buffer(WebGL2RenderingContext);
  config.spoof.webgl.parameter(WebGLRenderingContext);
  config.spoof.webgl.parameter(WebGL2RenderingContext);
  //
  document.documentElement.dataset.wgscriptallow = true;
};

function webglInject(seed) {
  var script_1 = document.createElement('script');
  script_1.textContent = '(' + inject + ')(' + seed + ')';
  document.documentElement.appendChild(script_1);

  if (document.documentElement.dataset.wgscriptallow !== 'true') {
    var script_2 = document.createElement('script');
    script_2.textContent = `{
    const iframes = window.top.document.querySelectorAll("iframe[sandbox]");
    for (var i = 0; i < iframes.length; i++) {
      if (iframes[i].contentWindow) {
        if (iframes[i].contentWindow.WebGLRenderingContext) {
          iframes[i].contentWindow.WebGLRenderingContext.prototype.bufferData = WebGLRenderingContext.prototype.bufferData;
          iframes[i].contentWindow.WebGLRenderingContext.prototype.getParameter = WebGLRenderingContext.prototype.getParameter;
        }
        if (iframes[i].contentWindow.WebGL2RenderingContext) {
          iframes[i].contentWindow.WebGL2RenderingContext.prototype.bufferData = WebGL2RenderingContext.prototype.bufferData;
          iframes[i].contentWindow.WebGL2RenderingContext.prototype.getParameter = WebGL2RenderingContext.prototype.getParameter;
        }
      }
    }
  }`;
    //
    window.top.document.documentElement.appendChild(script_2);
  }

  console.log('webgl.js injected');
}

export default webglInject;

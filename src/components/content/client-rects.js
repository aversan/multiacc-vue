var inject = function(seed) {
  const getClientRects = Element.prototype.getClientRects;
  //
  const deviceRadio = 0.5 * (0.5 * (0.5 * (0.5 * (seed + 1) + 1) + 1) + 1);

  Object.defineProperty(Element.prototype, 'getClientRects', {
    value: function() {
      const blockChildren = [...getClientRects.apply(this, arguments)];
      return (
        blockChildren.length > 0 &&
          (blockChildren[0] = new DOMRect(
            blockChildren[0].x,
            blockChildren[0].y,
            blockChildren[0].width * deviceRadio,
            blockChildren[0].height * deviceRadio
          )),
        blockChildren
      );
    }
  });
};

function rectsInject(seed) {
  var script_1 = document.createElement('script');
  script_1.textContent = '(' + inject + ')(' + seed + ')';
  document.documentElement.appendChild(script_1);

  if (document.documentElement.dataset.cbscriptallow !== 'true') {
    var script_2 = document.createElement('script');
    script_2.textContent = `{
        const iframes = window.top.document.querySelectorAll("iframe[sandbox]");
        for (var i = 0; i < iframes.length; i++) {
          if (iframes[i].contentWindow) {
            if (iframes[i].contentWindow.Element) {
              iframes[i].contentWindow.Element.getClientRects = Element.getClientRects;
            }
          }
        }
      }`;
    //
    window.top.document.documentElement.appendChild(script_2);
  }

  console.log('client-rects.js injected [' + seed + ']');
}

export default rectsInject;

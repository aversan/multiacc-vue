/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layouts/main'

const faqRouter = {
  path: '/faq',
  component: Layout,
  children: [
    {
      path: '',
      component: () => import('@/views/pages/faq'),
      name: 'faq',
      meta: { title: 'Помощь' }
    },
    {
      path: ':id',
      component: () => import('@/views/pages/faq'),
      name: 'faq-single',
      meta: { title: 'Помощь' }
    }
  ]
}
export default faqRouter

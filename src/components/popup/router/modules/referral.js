/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layouts/main'

const referralRouter = {
  path: '/referral',
  component: Layout,
  children: [
    {
      path: '',
      component: () => import('@/views/pages/referral'),
      name: 'Партнерская программа',
      meta: { title: 'Партнерская программа' }
    }
  ]
}
export default referralRouter

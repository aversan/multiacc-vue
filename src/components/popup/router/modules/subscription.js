/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layouts/main';

const subscriptionRouter = {
  path: '/subscription',
  component: Layout,
  children: [
    {
      path: '',
      component: () => import('@/views/pages/index'),
      name: 'Моя подписка',
      meta: { title: 'Моя подписка' }
    },
    {
      path: 'select',
      component: () => import('@/views/pages/subscribe'),
      meta: { title: 'Выбор тарифа' }
    }
  ]
};
export default subscriptionRouter;

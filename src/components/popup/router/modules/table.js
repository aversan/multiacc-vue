/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layouts/main'

const tableRouter = {
  path: '/table',
  component: Layout,
  children: [
    {
      path: 'faq',
      component: () => import('@/views/utility/faqs'),
      name: 'FAQ',
      meta: { title: 'FAQ' }
    },
    {
      path: 'pricing',
      component: () => import('@/views/utility/pricing'),
      name: 'Pricing',
      meta: { title: 'Pricing' }
    }
  ]
}
export default tableRouter

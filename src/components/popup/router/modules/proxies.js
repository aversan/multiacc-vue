/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layouts/main'

const proxiesRouter = {
  path: '/proxies',
  component: Layout,
  children: [
    {
      path: '',
      component: () => import('@/views/proxies/index'),
      name: 'Прокси',
      meta: { title: 'Прокси' }
    },
    {
      path: 'select',
      component: () => import('@/views/proxies/subscribe'),
      name: 'Выбор тарифа',
      meta: { title: 'Выбор тарифа' }
    }
  ]
}
export default proxiesRouter

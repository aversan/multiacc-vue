/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layouts/main';

const profilesRouter = {
  path: '/profiles',
  component: Layout,
  children: [
    {
      path: 'current',
      component: () => import('@/views/profiles/edit'),
      name: 'Текущий профиль',
      meta: { title: 'Текущий профиль' }
    },
    {
      path: '',
      component: () => import('@/views/profiles/edit'),
      name: 'Текущий профиль',
      meta: { title: 'Текущий профиль' }
    }
  ]
};
export default profilesRouter;

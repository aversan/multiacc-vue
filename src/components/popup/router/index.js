import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/* Layout */
import Layout from '@/layouts/main';

/* Router Modules */
// import tableRouter from './modules/table';
import subscriptionRouter from './modules/subscription';
import faqRouter from './modules/faq';
// import proxiesRouter from './modules/proxies';
import profilesRouter from './modules/profiles';
// import referralRouter from './modules/referral';

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/account/login')
  },
  {
    path: '/register',
    component: () => import('@/views/account/register')
  },
  {
    path: '/reset-password',
    component: () => import('@/views/account/reset-password')
  },
  {
    path: '/forgot-password',
    component: () => import('@/views/account/forgot-password')
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/account/auth-redirect')
  },
  {
    path: '/404',
    component: () => import('@/views/utility/404')
  },
  {
    path: '/401',
    component: () => import('@/views/utility/404')
  },
  {
    path: '/',
    component: Layout,
    redirect: '/profiles'
    // children: [
    //   {
    //     path: '',
    //     component: () => import('@/views/home'),
    //     name: 'Dashboard',
    //     meta: { title: 'Dashboard', icon: 'dashboard', affix: true }
    //   }
    // ]
  }
];

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  subscriptionRouter,
  faqRouter,
  // proxiesRouter,
  profilesRouter,
  // referralRouter,
  // tableRouter,
  {
    path: '/profile',
    component: Layout,
    children: [
      {
        path: '',
        component: () => import('@/views/account/profile'),
        name: 'Мой профиль',
        meta: { title: 'Мой профиль', icon: 'user', noCache: true }
      }
    ]
  },
  {
    path: '/contacts',
    component: Layout,
    children: [
      {
        path: '',
        component: () => import('@/views/pages/contacts'),
        name: 'Реквизиты и контакты',
        meta: { title: 'Реквизиты и контакты', icon: 'user', noCache: true }
      }
    ]
  },
  // {
  //   path: 'external-link',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'https://github.com/PanJiaChen/vue-element-admin',
  //       meta: { title: 'External Link', icon: 'link' }
  //     }
  //   ]
  // },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404' }
];

const createRouter = () =>
  new Router({
    mode: 'hash', // require service support
    scrollBehavior: (to, from, savedPosition) => {
      if (to.name === 'faq-single' || to.name === 'faq') {
        return savedPosition;
      }

      return { y: 0 };
    },
    routes: constantRoutes
  });

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;

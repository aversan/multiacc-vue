const domains = [
  'avito.ru',
  'avito.st',
  'youla.ru',
  'youla.io',
  'olx.ua',
  'olx.kz',
  'olx.uz'
];

export default domains;

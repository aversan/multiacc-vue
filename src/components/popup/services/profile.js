import browser from 'webextension-polyfill';
import domains from '@/domains';
import offsets from '@/timezone-offsets';
import store from '@/store';
import storage from '@/utils/storage';
import { saveProfile } from '@/api/profiles';
import { DateTime } from 'luxon';

export default {
  analyze(timezone) {
    const m = DateTime.local().setZone(timezone);
    const country = timezone.split('/')[1].replace(/[-_]/g, ' ');
    const storage = offsets[timezone] ? offsets[timezone] : { msg: {} };
    storage.msg = storage
      ? storage.msg
      : {
          standard: country + ' Standard Time',
          daylight: country + ' Daylight Time'
        };
    return {
      offset: m.offset,
      storage
    };
  },
  setTimezone(timezone = 'Europe/Moscow') {
    const { offset, storage } = this.analyze(timezone);
    console.log('setTimezone', timezone, offset, storage);

    localStorage.setItem('offset', offset);
    localStorage.setItem('isDST', offset !== storage.offset);
    localStorage.setItem('location', timezone);
    localStorage.setItem('daylight', storage.msg ? storage.msg.daylight : '');
    localStorage.setItem('standard', storage.msg ? storage.msg.standard : '');
  },
  async setCookies(cookies) {
    if (!cookies) {
      cookies = [];
    }

    const plan = store.getters['user/plan'];

    // Удаляем все текущие куки на доменах которые будут использоваться в профиле
    // чтобы при установке кук из профиля не осталось лишних кук
    for (const domain of domains) {
      // console.log('delete domain cookie', domain);
      const browserCookies = await browser.cookies.getAll({
        domain: domain
      });

      browserCookies.forEach(cookie => {
        let deleteCookie =
          (plan.duration >= 180 &&
            (-1 !== cookie.domain.indexOf('youla.ru') ||
              -1 !== cookie.domain.indexOf('youla.io') ||
              -1 !== cookie.domain.indexOf('olx'))) ||
          -1 !== cookie.domain.indexOf('avito.ru');

        if (deleteCookie) {
          let domain =
            cookie.domain.charAt(0) === '.'
              ? cookie.domain.substr(1)
              : cookie.domain;
          console.log(
            'delete cookies',
            cookie.name,
            cookie.domain,
            cookie.path,
            `https://${domain}${cookie.path}`
          );

          browser.cookies.remove({
            url: `https://${domain}${cookie.path}`,
            name: cookie.name,
            storeId: cookie.storeId
          });
        }
      });
    }

    console.log('all cookies deleted');

    for (const e of cookies) {
      let domain = e.domain.charAt(0) === '.' ? e.domain.substr(1) : e.domain;

      if (
        -1 !== e.domain.indexOf('youla.ru') ||
        -1 !== e.domain.indexOf('youla.io') ||
        -1 !== e.domain.indexOf('olx')
      ) {
        if (plan.duration < 180) continue;
      }

      let curDate = +new Date() / 1000;
      if (e.expirationDate < curDate) {
        e.expirationDate = curDate + 3600 * 24;
      }

      const t = {
        url: `https://${domain}${e.path}`,
        domain: e.domain,
        expirationDate: e.expirationDate,
        httpOnly: e.httpOnly,
        name: e.name,
        path: e.path,
        sameSite: e.sameSite,
        secure: e.secure,
        storeId: e.storeId,
        value: e.value
      };

      console.log('set cookie', t);
      browser.cookies.set(t);
    }
    console.log('setCookies done');
  },
  setProxy() {
    browser.runtime.sendMessage({ type: 'setproxy' });
  },

  async getCookies() {
    const saveCookies = [];
    for (const domain of domains) {
      // console.log('check domain cookie', domain);
      const cookies = await browser.cookies.getAll({
        domain: domain
      });

      cookies.forEach(cookie => {
        // console.log(cookie.domain);
        saveCookies.push(cookie);
      });
    }

    return saveCookies;
  },

  setUseragent(profile) {
    const seedrandom = require('seedrandom');
    const myrng = new seedrandom(profile.cid);

    const userAgents = [
      'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.53 Safari/525.19',
      'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.36 Safari/525.19',
      'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/7.0.540.0 Safari/534.10',
      'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.481.0 Safari/534.4',
      'Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US) AppleWebKit/533.4 (KHTML, like Gecko) Chrome/5.0.375.86 Safari/533.4',
      'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/532.2 (KHTML, like Gecko) Chrome/4.0.223.3 Safari/532.2',
      'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/4.0.201.1 Safari/532.0',
      'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.27 Safari/532.0',
      'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/530.5 (KHTML, like Gecko) Chrome/2.0.173.1 Safari/530.5',
      'Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.10 (KHTML, like Gecko) Chrome/8.0.558.0 Safari/534.10',
      'Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML,like Gecko) Chrome/9.1.0.0 Safari/540.0',
      'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/534.14 (KHTML, like Gecko) Chrome/9.0.600.0 Safari/534.14',
      'Mozilla/5.0 (X11; U; Windows NT 6; en-US) AppleWebKit/534.12 (KHTML, like Gecko) Chrome/9.0.587.0 Safari/534.12',
      'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.13 (KHTML, like Gecko) Chrome/9.0.597.0 Safari/534.13',
      'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.11 Safari/534.16',
      'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/534.20 (KHTML, like Gecko) Chrome/11.0.672.2 Safari/534.20',
      'Mozilla/5.0 (Windows NT 6.0) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.792.0 Safari/535.1',
      'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.872.0 Safari/535.2',
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7',
      'Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.66 Safari/535.11',
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.45 Safari/535.19',
      'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24',
      'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6',
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1',
      'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.15 (KHTML, like Gecko) Chrome/24.0.1295.0 Safari/537.15',
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1467.0 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1623.0 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.116 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36',
      'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.38 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
      'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.62 Safari/537.36'
    ];

    let ua = userAgents[Math.floor(myrng() * userAgents.length - 1)];

    const version = Math.floor(myrng() * 15) + 80;
    const subVersion = Math.floor(myrng() * 4000) + 1000;
    const subSubVersion = Math.floor(myrng() * 150);

    ua = ua.replace(
      /Chrome\/(\d+)\.(\d+)\.(\d+)\.(\d+)/,
      `Chrome/${version}.0.${subVersion}.${subSubVersion}`
    );
    ua = ua.replace(/Safari\/(\d+)\.(\d+)/, 'Safari/537.36');

    storage.set('useragent', ua);
    console.log('set useragent', JSON.stringify(profile), ua);
  },

  async apply(profile) {
    
    const currentProfile = store.getters['profiles/currentProfile'];
    if (currentProfile && profile.id !== currentProfile.id) {
      await this.saveCurrent();
    } else {
      console.log('re-applying current profile');
    }

    console.log('ready to apply', JSON.stringify(profile));
    await store.dispatch(
      'profiles/setProfile',
      JSON.parse(JSON.stringify(profile))
    );

    if (profile.data.geo && profile.data.geo.time_zone) {
      this.setTimezone(profile.data.geo.time_zone.name);
    }

    if (profile.data.geo) {
      localStorage['lat'] = profile.data.geo.lat;
      localStorage['lng'] = profile.data.geo.lng;
      browser.runtime.sendMessage({ type: 'activeChanged' });
    } else {
      localStorage['lat'] = 52.3;
      localStorage['lng'] = 6.2;
      browser.runtime.sendMessage({ type: 'activeChanged' });
    }

    await this.setCookies(profile.data.cookies);
    this.setProxy(profile.data);
    this.setUseragent(profile.data);
  },
  async create() {
    let defaultData = {
      name: 'Профиль (' + Math.round(999 * Math.random()) + ')',
      cookies: [],
      cid: Math.round(11703500 + 1e10 * Math.random()),
      folder:'',
      proxy_host: '',
      proxy_port: '',
      proxy_user: '',
      proxy_pass: '',
      geo: { time_zone: { name: 'Europe/Moscow' } }
    };
    let response = await saveProfile(defaultData);
    let newProfile = response.data;

    await this.apply(newProfile);
  },

  async saveCurrent() {
    console.log('saving current');
    let saveCookies = await this.getCookies();
    const currentProfile = store.getters['profiles/currentProfile'];
    if (currentProfile) {
      console.log('saving currentProfile', JSON.stringify(currentProfile));
      currentProfile.data.cookies = saveCookies;
      let response = await saveProfile(currentProfile.data, currentProfile.id);
      currentProfile.data = response.data.data;
      await store.dispatch('profiles/setProfile', currentProfile);
    }
  }
};

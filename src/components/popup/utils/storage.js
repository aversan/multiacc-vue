import browser from 'webextension-polyfill';
import store from '@/store';

const storage = (function() {
  console.log('storage loaded');
  var objs = {};

  return {
    load() {
      let data = browser.storage.local.get(null);
      data.then(o => {
        objs = o;
        console.log('storage loaded 1', JSON.stringify(objs));

        store.dispatch('user/loginWithToken', objs['token']);
        if (objs['server']) {
          store.dispatch('app/set_server', objs['server']);
        } else {
          store.dispatch('app/set_server', process.env.VUE_APP_BASE_API);
        }
      });

      return data;
    },
    get: function(id) {
      let data;
      data = objs[id];
      console.log('storage get ', id, data);
      return data;
    },
    set: function(id, data) {
      try {
        objs[id] = JSON.parse(JSON.stringify(data));
      } catch (e) {
        if (data) {
          objs[id] = data;
        }
      }
      console.log('storage set ', id, objs[id], objs);
      browser.storage.local
        .set(objs)
        .then(asd => {
          console.log('storage set ok ', id, objs[id], asd);
        })
        .catch(e => {
          console.error('storage set error ', id, objs[id], e);
        });
    },
    remove: function(id) {
      delete objs[id];
      browser.storage.local.set(objs);
    }
  };
})();

export default storage;

import Vue from 'vue'
// import Clipboard from 'clipboard'

function clipboardSuccess(text) {
  Vue.prototype.$swal({
    toast: true,
    position: 'top',
    showConfirmButton: false,
    timer: 3000,
    type: 'info',
    title: 'Скопировано: ' + text
  })
}

export default function handleClipboard(text) {
  const el = document.createElement('textarea')
  el.value = text
  el.setAttribute('readonly', '')
  el.style.position = 'absolute'
  el.style.left = '-9999px'
  document.body.appendChild(el)
  el.select()
  document.execCommand('copy')
  document.body.removeChild(el)

  clipboardSuccess(text)
}

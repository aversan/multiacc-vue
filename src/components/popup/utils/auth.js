// import browser from 'webextension-polyfill';
import storage from '@/utils/storage';

const TokenKey = 'token';

export function getToken() {
  return storage.get(TokenKey);
}

export function setToken(token) {
  storage.set(TokenKey, token);
  return storage.get(TokenKey);
}

export function removeToken() {
  return storage.remove(TokenKey);
}

// const TokenKey = 'Admin-Token';
//
// export function getToken() {
//   console.log('getToken', Cookies.get(TokenKey));
//
//   return Cookies.get(TokenKey);
// }
//
// export function setToken(token) {
//   console.log('setToken', JSON.stringify(token));
//
//   return Cookies.set(TokenKey, token, { expires: 30 * 6 });
// }
//
// export function removeToken() {
//   return Cookies.remove(TokenKey);
// }

/*
// import Cookies from 'js-cookie'
import browser from 'webextension-polyfill';

const TokenKey = 'token';

export async function getToken() {
  let data = await browser.storage.local.get(TokenKey);
  console.log(data[TokenKey]);
  return data[TokenKey];
}

export async function setToken(token) {
  console.log('setToken', JSON.stringify(token));

  return await browser.storage.local.set({ token });
}

export async function removeToken() {
  console.log('removeToken');
  return await browser.storage.local.remove(TokenKey);
}

 */

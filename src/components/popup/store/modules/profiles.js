import storage from '@/utils/storage';

const state = {
  currentProfile: null
};

const getters = {
  currentProfile: state => state.currentProfile
};

const mutations = {
  SET_PROFILE: (state, profile) => {
    console.log('SET_PROFILE', profile);
    state.currentProfile = profile;
    storage.set('profile', profile);
  }
};

const actions = {
  setProfile({ commit }, profile) {
    console.log('setProfile', profile);
    commit('SET_PROFILE', profile);
  },
  async loadProfile({ commit }) {
    let data = storage.get('profile');
    if (data && data.id) {
      commit('SET_PROFILE', data);
    } else {
      console.log('no profile selected');
    }
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};

import storage from '@/utils/storage';

const state = {
  loadingTitle: null,
  loading: false,
  server: process.env.VUE_APP_BASE_API
};

const getters = {
  loading: state => state.loading,
  loadingTitle: state => state.loadingTitle,
  server: state => state.server
};

const mutations = {
  SET_LOADING: (state, { isLoading, title }) => {
    state.loading = isLoading;
    state.loadingTitle = title;
  },
  SET_SERVER: (state, { server }) => {
    state.server = server;
    storage.set('server', server);
  }
};

const actions = {
  loading({ commit }, title) {
    commit('SET_LOADING', { isLoading: true, title });
  },
  loadingDone({ commit }) {
    commit('SET_LOADING', { isLoading: false, title: null });
  },
  set_server({ commit }, server) {
    commit('SET_SERVER', { server });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};

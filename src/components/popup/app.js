/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import '@/sass/app.scss';

require('./bootstrap');
import Vue from 'vue';
// import * as Sentry from '@sentry/browser';
// import { Vue as VueIntegration } from '@sentry/integrations';
// import VueYandexMetrika from 'vue-yandex-metrika';

// if (process.env.NODE_ENV === 'production') {
//   console.log(process.env.NODE_ENV);
//   Sentry.init({
//     dsn:
//       'https://3bd4d53f0ed24d138c2db90c5ddb6556@o428910.ingest.sentry.io/5374757',
//     integrations: [new VueIntegration({ Vue, attachProps: true })]
//   });
// }

window.Vue = Vue;

import {
  BadgePlugin,
  OverlayPlugin,
  DropdownPlugin,
  TablePlugin,
  CollapsePlugin,
  ImagePlugin,
  LayoutPlugin,
  FormPlugin,
  FormRadioPlugin,
  FormTextareaPlugin,
  FormCheckboxPlugin,
  FormInputPlugin,
  PaginationPlugin,
  FormGroupPlugin,
  FormSelectPlugin,
  ModalPlugin,
  ButtonPlugin,
  AlertPlugin,
  TooltipPlugin,
  SidebarPlugin,
  SpinnerPlugin,
  InputGroupPlugin
} from 'bootstrap-vue';

[
  BadgePlugin,
  OverlayPlugin,
  DropdownPlugin,
  TablePlugin,
  CollapsePlugin,
  LayoutPlugin,
  ImagePlugin,
  FormPlugin,
  FormRadioPlugin,
  FormInputPlugin,
  PaginationPlugin,
  FormTextareaPlugin,
  FormCheckboxPlugin,
  FormGroupPlugin,
  FormSelectPlugin,
  ModalPlugin,
  ButtonPlugin,
  AlertPlugin,
  TooltipPlugin,
  SidebarPlugin,
  SpinnerPlugin,
  InputGroupPlugin
].forEach(x => Vue.use(x));

import vClickOutside from 'v-click-outside';
import VueMask from 'v-mask';
// import VueSweetalert2 from 'vue-sweetalert2';
import {
  ValidationProvider,
  ValidationObserver,
  extend,
  localize
} from 'vee-validate';
import ru from 'vee-validate/dist/locale/ru.json';
import * as rules from 'vee-validate/dist/rules';

import App from './App.vue';
import store from './store';
import router from './router';

import './middlewares/permission';

import storage from '@/utils/storage'; // permission control

Vue.prototype.$isDev = process.env.MIX_APP_ENV !== 'production';
Vue.prototype.$isDev = true;
Vue.prototype.$globalFolders=[];
Vue.config.devtools = Vue.prototype.$isDev;
Vue.config.debug = Vue.prototype.$isDev;
Vue.config.silent = !Vue.prototype.$isDev;
Vue.config.productionTip = false;

// Vue.use(VueYandexMetrika, {
//   id: 52298116,
//   router: router,
//   env: process.env.NODE_ENV
// });

Vue.use(vClickOutside);
Vue.use(VueMask);
// Vue.use(require('vue-chartist'));
// Vue.use(VueSweetalert2);

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

Object.keys(rules).forEach(rule => {
  extend(rule, rules[rule]);
});

ru.messages.confirmed = 'Поле {_field_} не совпадает с {target}';
localize('ru', ru);

storage.load().then(() => {
  console.log('initialize app');
  new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  });
});

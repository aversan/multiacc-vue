import request from '@/utils/request'

export function getPlans() {
  return request({
    url: '/api/v2/subscription/plans',
    method: 'get'
  })
}

export function getPlan(id) {
  return request({
    url: `/api/v2/subscription/plans/${id}`,
    method: 'get'
  })
}

export function getDiscount(price, code) {
  return request({
    url: '/api/v2/subscription/promocode',
    method: 'get',
    params: {
      price,
      code
    }
  })
}

export function paymentData() {
  return request({
    url: '/api/v2/subscription/payment',
    method: 'get'
  })
}

export function getSubscriptionInfo() {
  return request({
    url: '/api/v2/subscription/info',
    method: 'get'
  })
}

export function getProxyPlans() {
  return request({
    url: '/api/v2/subscription/proxy/plans',
    method: 'get'
  })
}

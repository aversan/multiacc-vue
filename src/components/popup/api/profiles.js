import request from '@/utils/request';

export function getProfiles(params) {
  return request({
    url: '/api/v2/profiles',
    method: 'get',
    params
  });
}

export function saveProfile(data, id) {
  return request({
    url: `/api/v2/profiles/${id || ''}`,
    method: 'post',
    data
  });
}

export function importProfiles(data) {
  return request({
    url: `/api/v2/profiles/import`,
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
}

export function exportProfiles() {
  return request({
    url: `/api/v2/profiles/export`,
    method: 'get',
    responseType: 'blob'
  });
}

import request from '@/utils/request';

export function getProxies(params) {
  return request({
    url: '/api/v2/proxies',
    method: 'get',
    params
  });
}

export function getTrial(params) {
  return request({
    url: '/api/v2/proxies/trial',
    method: 'get',
    params
  });
}

export function getProxyStatus(id) {
  return request({
    url: `/api/v2/proxies/${id}/status`,
    method: 'get'
  });
}

export function proxyIPSwitch(id) {
  return request({
    url: `/api/v2/proxies/${id}/switch`,
    method: 'get'
  });
}

export function proxyIPSwitchAnon(proxy_user, proxy_pass) {
  return request({
    url: `/api/proxy/switch?user=${proxy_user}&password=${proxy_pass}`,
    method: 'get'
  });
}

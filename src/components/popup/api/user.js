import request from '@/utils/request'

export function enableCSRF() {
  return request({
    url: '/sanctum/csrf-cookie',
    method: 'get'
  })
}

export function login(data) {
  return request({
    url: '/api/v2/user/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/api/v2/user/info',
    method: 'get'
  })
}

export function saveInfo(data) {
  return request({
    url: `/api/v2/user/info`,
    method: 'post',
    data,
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

export function logout() {
  return request({
    url: '/api/v2/user/logout',
    method: 'post'
  })
}

export function getReferralData() {
  return request({
    url: '/api/v2/user/referral',
    method: 'get'
  })
}

export function register(data) {
  return request({
    url: '/api/v2/user/register',
    method: 'post',
    data
  })
}

export function restore(data) {
  return request({
    url: 'password/email',
    method: 'post',
    data
  })
}

export function reset(data) {
  return request({
    url: '/password/reset',
    method: 'post',
    data
  })
}

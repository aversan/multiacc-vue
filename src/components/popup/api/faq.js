import request from '@/utils/request'

export function getAnswers(category, params) {
  return request({
    url: `/api/v2/faq/${category}`,
    method: 'get',
    params
  })
}

export function getAnswer(answer, params) {
  return request({
    url: `/api/v2/faq/answer/${answer}`,
    method: 'get',
    params
  })
}

import router from '@/router';
import store from '@/store';
import NProgress from 'nprogress'; // progress bar
import 'nprogress/nprogress.css'; // progress bar style
import { getToken } from '@/utils/auth'; // get token from cookie
import getPageTitle from '@/utils/get-page-title';
import checkSubscription from '@/middlewares/subscription';

NProgress.configure({ showSpinner: false }); // NProgress Configuration

const whiteList = [
  '/register',
  '/login',
  '/auth-redirect',
  '/reset-password',
  '/forgot-password'
]; // no redirect whitelist

let loaded = false;
async function loadinfo(to, from, next) {
  console.log('loadinfo');
  try {
    // get user info
    // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
    await store.dispatch('user/getInfo');

    // generate accessible routes map based on roles
    const accessRoutes = await store.dispatch('permission/generateRoutes', [
      'user'
    ]);

    router.beforeEach(checkSubscription);
    // dynamically add accessible routes
    router.addRoutes(accessRoutes);

    loaded = true;
  } catch (error) {
    // remove token and go to login page to re-login
    await store.dispatch('user/resetToken');
    // alert(error || 'Has Error');
    next(`/login`);
    NProgress.done();
  }
}

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start();

  // set page title
  document.title = getPageTitle(to.meta.title);

  // determine whether the user has logged in
  const hasToken = await getToken();

  console.log('hasToken', hasToken);
  if (hasToken) {
    console.log('hasToken yes', JSON.stringify(hasToken));

    if (!loaded) {
      loadinfo(to, from, next);
    }

    if (to.path === '/login') {
      // if is logged in, redirect to the home page
      next({ path: '/profiles' });
      NProgress.done(); // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
    } else {
      console.log('not login');

      // determine whether the user has obtained his permission roles through getInfo
      // const hasRoles = store.getters.roles && store.getters.roles.length > 0;
      // if (hasRoles) {
      console.log('has roles');

      next();
      NProgress.done();

      // }
    }
  } else {
    /* has no token*/

    if (whiteList.indexOf(to.path) !== -1) {
      console.log('/no token whitelist');

      // in the free login whitelist, go directly
      next();
    } else {
      console.log('/no token');

      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`);
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});

import store from '@/store';

const whiteList = [
  '/register',
  '/login',
  '/auth-redirect',
  '/reset-password',
  '/forgot-password',
  '/subscription',
  '/subscription/select'
]; // no redirect whitelist

async function checkSubscription(to, from, next) {
  if (whiteList.indexOf(to.path) !== -1) {
    console.log('subscribe whitelist');
    next();
  } else {
    let isActive = await store.getters['user/is_active'];

    if (isActive) {
      next();
    } else {
      next({ path: '/subscription/select', query: { subscribe: 'now' } });
    }
  }
}

export default checkSubscription;

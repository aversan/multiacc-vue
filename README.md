# multiacc-extension


## web-ext setup
```
npm install --global web-ext
```


## web-ext run
```
web-ext run --source-dir ./dist/
```


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

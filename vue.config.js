const path = require('path');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//   .BundleAnalyzerPlugin;

module.exports = {
  filenameHashing: false,

  pages: {
    // index: {
    //   // точка входа для страницы
    //   entry: "src/index/main.js",
    //   // исходный шаблон
    //   template: "public/index.html",
    //   // в результате будет dist/index.html
    //   filename: "index.html",
    //   // когда используется опция title, то <title> в шаблоне
    //   // должен быть <title><%= htmlWebpackPlugin.options.title %></title>
    //   title: "Index Page",
    //   // все фрагменты, добавляемые на этой странице, по умолчанию
    //   // это извлечённые общий фрагмент и вендорный фрагмент.
    //   chunks: ["chunk-vendors", "chunk-common", "index"]
    // },
    // когда используется строковый формат точки входа, то
    // шаблон будет определяться как `public/subpage.html`,
    // а если таковой не будет найден, то `public/index.html`.
    // Выходное имя файла будет определено как `subpage.html`.
    backend: 'src/components/backend/index.js',
    // content: 'src/components/content/inject.js',
    index: 'src/components/popup/app.js',
    popup: 'src/components/popup/app.js'
  },
  chainWebpack: config => {
    config.resolve.alias.set('@', path.resolve('src/components/popup'));

    // config.optimization.delete("splitChunks"); // no vendor chunks
    // config.plugins.delete("prefetch"); // no prefetch chunks
    // config.plugins.delete("preload"); // no preload chunks

    // config.plugin('webpack-report').use(BundleAnalyzerPlugin, [
    //   {
    //     // ...webpack-bundle-analyzer options here
    //   }
    // ]);

    config.plugin('copy').tap(args => {
      args[0].push({
        from: path.resolve(__dirname, 'src/static'),
        to: path.resolve(__dirname, 'dist'),
        toType: 'dir',
        ignore: ['.DS_Store']
      });
      return args;
    });

    config.entry('content').add('./src/components/content/inject.js');
  }
};
